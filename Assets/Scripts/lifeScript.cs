﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lifeScript : MonoBehaviour {

	Animator my_animation;
	public bool is_full;
	public bool is_visible;

	void Start () {
		my_animation = gameObject.GetComponent<Animator> ();
	}
	
	public void addLife() {
		my_animation.Play ("lifeAdd");
		is_full = true;
		is_visible = true;
	}

	public void addLifeMax() {
		my_animation.Play ("lifeEmptyAdd");
		is_full = false;
		is_visible = true;
	}

	public void removeLife() {
		my_animation.Play ("lifeRemove");
		is_full = false;
	}

	public void removeLifeMax() {
		if (is_full) {
			my_animation.Play ("lifeMaxRemove");
			is_full = false;
		} else {
			my_animation.Play ("lifeEmptyRemove");
			is_visible = false;
		}
	}
}
