﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class eggScript : MonoBehaviour {

    public int spawnPoint = 0;  // 1 to 4 according to its spawn coop.
    public float velocity = 0;    // Egg rolling speed.
    public float acceleration = 0;    // Egg acceleration.
	public int id = 0;
    public int egg_type = 0;    
	// 0 for normal egg (1 Point);
	// 1 for regen egg.
	// 2 for max life egg.
	// 3 for rotten egg (-1 life);
	// 4 for stinky egg (-1 max life);
	// 5 for big egg (5 Points); 
	// 6 for super egg (20 Points).
	// 7 Multiplier egg. Increases multiplier by 1.
	// 8 Multiplier egg. Increases multiplier by 3.
	// 9 Multiplier egg. Increases multiplier by 10.
	// 10 Decreases multiplier by 1.
	// 11 Decreases multiplier by 3.
	// 12 Decreases multiplier to 1.
	// 13 Low Gravity egg.
	// 14 SUPER MODE Egg. Spawn rates and gravity double. No penalty from dropping eggs.
    public int self_destruct = -1;
	// public Color32 egg_color = new Color32(255, 255, 255, 255);
	public Sprite splatterType;

	public void Start() {
		switch (egg_type) {
		case 0:
			// egg_color = new Color32(255, 255, 255, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/1");
			splatterType = Resources.Load<Sprite>("visual/b1");
			break;
		case 1:
			// egg_color = new Color32(65, 255, 21, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/4");
			splatterType = Resources.Load<Sprite>("visual/b4");
			break;
		case 2:
			// egg_color = new Color32(113, 167, 21, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/5");
			splatterType = Resources.Load<Sprite>("visual/b5");
			break;
		case 3:
			// egg_color = new Color32(194, 90, 90, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/6");
			splatterType = Resources.Load<Sprite>("visual/b6");
			break;
		case 4:
			// egg_color = new Color32(0, 0, 0, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/7");
			splatterType = Resources.Load<Sprite>("visual/b7");
			break;
		case 5:
			// egg_color = new Color32(255, 243, 199, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/2");
			splatterType = Resources.Load<Sprite>("visual/b2");
			break;
		case 6:
			// egg_color = new Color32(255, 226, 141, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/3");
			splatterType = Resources.Load<Sprite>("visual/b3");
			break;
		case 7:
			// egg_color = new Color32(216, 219, 255, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/8");
			splatterType = Resources.Load<Sprite>("visual/b8");
			break;
		case 8:
			// egg_color = new Color32(165, 176, 255, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/9");
			splatterType = Resources.Load<Sprite>("visual/b9");
			break;
		case 9:
			// egg_color = new Color32(111, 115, 255, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/10");
			splatterType = Resources.Load<Sprite>("visual/b10");
			break;
		case 10:
			// egg_color = new Color32(255, 173, 173, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/11");
			splatterType = Resources.Load<Sprite>("visual/b11");
			break;
		case 11:
			// egg_color = new Color32(255, 118, 118, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/12");
			splatterType = Resources.Load<Sprite>("visual/b12");
			break;
		case 12:
			// egg_color = new Color32(255, 53, 53, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/13");
			splatterType = Resources.Load<Sprite>("visual/b13");
			break;
		case 13:
			// egg_color = new Color32(116, 243, 255, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/14");
			splatterType = Resources.Load<Sprite>("visual/b14");
			break;
		case 14:
			// egg_color = new Color32(255, 167, 249, 255);
			this.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("visual/15");
			splatterType = Resources.Load<Sprite>("visual/b15");
			break;
		default:
			break;
		}
	}

    private void FixedUpdate() {
        if (self_destruct == 0)
            Destroy(this.gameObject);
        else if (self_destruct > 0)
            self_destruct--;
    }

}
