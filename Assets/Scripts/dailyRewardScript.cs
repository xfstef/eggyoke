﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dailyRewardScript : MonoBehaviour {

	public GameObject days, lives;

	public void setDays(int amount) {
		days.GetComponent<Text>().text = amount.ToString ();
	}

	public void setLives(int amount) {
		if (amount == 4)
			lives.GetComponent<Text> ().text = amount.ToString () + " (MAX Bonus)";
		else
			lives.GetComponent<Text>().text = amount.ToString ();
	}

}
