﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class currentLives : MonoBehaviour {

    public int lives = 3;
	public int maxLives = 3;
	GameObject[] all_lives;
	lifeScript[] all_life_scripts; 

	void Start() {
		all_lives = new GameObject[7];
		all_lives = GameObject.FindGameObjectsWithTag("life");
		Array.Sort(all_lives, delegate (GameObject life1, GameObject life2) { return life1.name.CompareTo(life2.name); });
		all_life_scripts = new lifeScript[7];
		for (int x = 0; x < 7; x++) {
			all_life_scripts [x] = all_lives [x].GetComponent<lifeScript> ();
		}
	}

	public void initializeLives(int amount) {
		lives = amount;
		maxLives = amount;
		for (int x = 0; x < 7; x++) {
			if (x < amount) 
				all_life_scripts [x].addLife ();
			else
				if(all_life_scripts[x].is_visible)
					all_life_scripts [x].removeLifeMax ();
		}
	}

	public void afterAd() {
		lives = 3;
		all_life_scripts [0].addLife ();
		all_life_scripts [1].addLife ();
		all_life_scripts [2].addLife ();
	}

	public void addLife(){
		all_life_scripts [lives].addLife ();
		lives++;
	}

	public void addLifeMax() {
		all_life_scripts [maxLives].addLifeMax();
		maxLives++;
	}

	public void removeLife() {
		lives--;
		all_life_scripts [lives].removeLife ();
	}

	public void removeLifeMax() {
		maxLives--;
		all_life_scripts [maxLives].removeLifeMax ();
		if (maxLives < lives) {
			lives--;
		}
	}
}
