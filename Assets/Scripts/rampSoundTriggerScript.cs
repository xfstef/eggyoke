﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rampSoundTriggerScript : MonoBehaviour {

	AudioSource my_source;
	public int my_ramp_nr;

	void Start() {
		my_source = gameObject.GetComponent<AudioSource>();
	}

	void OnTriggerEnter2D(Collider2D col) {
		my_source.Play ();
		if(col.gameObject.GetComponent<eggScript>() != null)
			col.gameObject.GetComponent<eggScript> ().spawnPoint = my_ramp_nr;
	}
}
