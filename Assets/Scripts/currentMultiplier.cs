﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class currentMultiplier : MonoBehaviour {

    public int multiplier = 1;   // Just the Multiplier.
    public float egg_frequency = 1;

	public void updateMultiplier() {  // Returns true if game over!
		this.gameObject.GetComponent<Text>().text = multiplier.ToString();
	}

	public int increaseMultiplier(int amount) {
		if (multiplier == 20)
			return 9999;
		multiplier += amount;
		if (multiplier > 20) {
			multiplier = 20;
			return 9999;
		}
		return returnEggs ();
	}

	public int decreaseMultiplier(int amount) {
		if (multiplier == 1)
			return 5;
		multiplier -= amount;
		if (multiplier < 1) {
			multiplier = 1;
			return 5;
		}

		return returnEggs ();
	}

	int returnEggs() {
		switch (multiplier) {
		case 1:
			return 5;
		case 2:
			return 6;
		case 3:
			return 7;
		case 4:
			return 8;
		case 5:
			return 9;
		case 6:
			return 10;
		case 7:
			return 11;
		case 8:
			return 12;
		case 9:
			return 13;
		case 10:
			return 15;
		case 11:
			return 17;
		case 12:
			return 19;
		case 13:
			return 21;
		case 14:
			return 23;
		case 15:
			return 26;
		case 16:
			return 29;
		case 17:
			return 32;
		case 18:
			return 36;
		case 19:
			return 41;
		default:
			return 9999;
		}
	}
}
