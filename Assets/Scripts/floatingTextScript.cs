﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class floatingTextScript : MonoBehaviour {

	public AudioClip low_gravity;
	public AudioClip super_mode;
	public AudioClip one_up;
	public AudioClip one_max_up;
	public AudioClip valuable_egg;
	public AudioClip multiplier_up;
	public AudioClip multiplier_down;
	public AudioClip one_down;
	public AudioClip one_max_down;
	AudioSource my_source;
	Text my_mesh;
	float death_timer;
	public int text_case;

	void Start() {
		my_source = gameObject.GetComponent<AudioSource>();
		my_mesh = gameObject.GetComponent<Text>();

		switch (text_case) {
		case 0:
			my_source.clip = low_gravity;
			my_mesh.text = "Time Warp!";
			death_timer = 1.5f;
			break;
		case 1:
			my_source.clip = super_mode;
			my_mesh.text = "SUPER!";
			death_timer = 2.5f;
			break;
		case 2:
			my_source.clip = one_up;
			my_mesh.text = "+1 Life!";
			death_timer = 1.5f;
			break;
		case 3:
			my_source.clip = one_max_up;
			my_mesh.text = "+1 Max Lives!";
			death_timer = 1.5f;
			break;
		case 4:
			my_source.clip = valuable_egg;
			my_mesh.text = "5 Points!";
			death_timer = 1.55f;
			break;
		case 40:
			my_source.clip = valuable_egg;
			my_mesh.text = "20 Points!";
			death_timer = 1.55f;
			break;
		case 5:
			my_source.clip = multiplier_up;
			my_mesh.text = "+1 Multiplier!";
			death_timer = 1.6f;
			break;
		case 50:
			my_source.clip = multiplier_up;
			my_mesh.text = "+3 Multiplier!";
			death_timer = 1.6f;
			break;
		case 51:
			my_source.clip = multiplier_up;
			my_mesh.text = "+10 Multiplier!";
			death_timer = 1.6f;
			break;
		case 6:
			my_source.clip = multiplier_down;
			my_mesh.text = "-1 Multiplier!";
			death_timer = 1.6f;
			break;
		case 60:
			my_source.clip = multiplier_down;
			my_mesh.text = "-2 Multiplier!";
			death_timer = 1.6f;
			break;
		case 61:
			my_source.clip = multiplier_down;
			my_mesh.text = "-3 Multiplier!";
			death_timer = 1.6f;
			break;
		case 62:
			my_source.clip = multiplier_down;
			my_mesh.text = "Multiplier Reset!";
			death_timer = 1.6f;
			break;
		case 7:
			my_source.clip = one_down;
			my_mesh.text = "-1 Life!";
			death_timer = 1.5f;
			break;
		case 8:
			my_source.clip = one_max_down;
			my_mesh.text = "-1 Max Lives!";
			death_timer = 1.5f;
			break;
		case 9:
			my_source.clip = one_max_down;
			my_mesh.text = "3";
			death_timer = 1f;
			break;
		case 90:
			my_source.clip = one_max_down;
			my_mesh.text = "2";
			death_timer = 1f;
			break;
		case 91:
			my_source.clip = one_max_down;
			my_mesh.text = "1";
			death_timer = 1f;
			break;
		case 92:
			my_source.clip = one_max_down;
			my_mesh.text = "GO!";
			death_timer = 1f;
			break;
		}

		my_source.Play ();
		Destroy (gameObject, death_timer);
	}
}
