﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using CloudOnce;

public class gameScript : MonoBehaviour {

	highScoreScript hS;
    currentLives cL;
	currentMultiplier cM;
    GameObject[] coops;
    coopScript[] coopScripts;
    floorScript floor;
    List<GameObject> spawnedEggs;
    public GameObject egg;
    public Canvas gameScreen, menu;
	public GameObject tipsPanel, gameSwitchPanel, dailyRewardPanel, menuButton;
    float screen_width_adjuster, screen_height_adjuster;
    int egg_spawn_break = 0;
    int eggs_needed = 5;
    bool is_paused = true;
    bool new_game = true;
	int current_spawn_rate = 100;
	int total_eggs_caught = 0;
	Text gSwitchT;
	Text gScoreT;
	Text highScoreT;
	int currentGlobalEffect = 0;
	// 0 for normal game mode.
	// 1 for half gravity.
	// 2 for super mode.
	int cGETimer = 0;
	int global_egg_ids = 0;
	int high_score = 0;
	int day_streak_lives = 3;
	DateTime last_date_played;
	DateTime today = System.DateTime.Now;
	bool adEnabled = true;
	Button ad_button;
	int updates_since_start = 0;
	public GameObject floating_text;
	float gravityScale = 30;
	gameSwitchButtonScript gameSwitchButton;
	int coop_animation_trigger;
	public GameObject multiplierTextPlaceholder;
	public Slider volume_slider;

    // Use this for initialization
    void Start () {
		currentGlobalEffect = 0;
		gameSwitchButton = GameObject.FindGameObjectWithTag ("gameSwitchButton").GetComponent<gameSwitchButtonScript>();
		gameSwitchButton.changeToNewGame (false);
		gSwitchT = GameObject.FindGameObjectWithTag ("gameSwitchText").GetComponent<Text> ();
		gScoreT = GameObject.FindGameObjectWithTag ("currentScore").GetComponent<Text> ();
		highScoreT = GameObject.FindGameObjectWithTag ("highScore").GetComponent<Text> ();
		hS = GameObject.FindGameObjectWithTag("score").GetComponent<highScoreScript>();
		hS.score = 0;
		hS.updateScore ();
        cL = GameObject.FindGameObjectWithTag("lives").GetComponent<currentLives>();
		cM = GameObject.FindGameObjectWithTag("multiplier").GetComponent<currentMultiplier>();
		cM.decreaseMultiplier (19);
		cM.updateMultiplier ();
        floor = GameObject.FindGameObjectWithTag("floor").GetComponent<floorScript>();
		floor.game_script = this;
        coops = new GameObject[4];
        coops = GameObject.FindGameObjectsWithTag("coop");
        Array.Sort(coops, delegate (GameObject coop1, GameObject coop2) { return coop1.name.CompareTo(coop2.name); });
        coopScripts = new coopScript[4];
        for(int x = 0; x < 4; x++) {
            coopScripts[x] = coops[x].GetComponent<coopScript>();
        }
		ad_button = GameObject.FindGameObjectWithTag ("showAdButton").GetComponent<Button> ();
        spawnedEggs = new List<GameObject>();
        screen_width_adjuster = (float)(Screen.width / 2560.0);
        screen_height_adjuster = (float)(Screen.height / 1440.0);

        is_paused = false;
		total_eggs_caught = 0;
		global_egg_ids = 0;
		high_score = PlayerPrefs.GetInt("High Score");
		highScoreT.text = ("High Score " + high_score);
		calculateDaysForDaily ();
		floor.ignore_floor = false;
		adEnabled = true;
		updates_since_start = 0;
		if (dailyRewardPanel.activeSelf)
			Time.timeScale = 0f;
		else
			Time.timeScale = 1f;
		gravityScale = 30;
		current_spawn_rate = 100;
		today = System.DateTime.Now;
        if (PlayerPrefs.HasKey("volume"))
            AudioListener.volume = PlayerPrefs.GetFloat("volume");
        else
            AudioListener.volume = 1;
        volume_slider.value = AudioListener.volume;
    }
	
	// FixedUpdate is called 60 times per second.
	void FixedUpdate () {
        if (!is_paused) {
			updates_since_start++;
			if (floor.decrease_cash) {
				cL.removeLife ();
				eggs_needed = cM.decreaseMultiplier (2);
				cM.updateMultiplier ();
				floor.decrease_cash = false;
			}
			if (cGETimer > 0) {
				cGETimer--;
				switch (currentGlobalEffect) {
				case 1:
					if (cGETimer == 0) {
						currentGlobalEffect = 0;
						floor.ignore_floor = false;
						adjustDifficulty ();
					}
					break;
				case 2:
					if (cGETimer > 300) {
						egg_spawn_break--;
						egg_spawn_break--;
					} else 
						currentGlobalEffect = 3;
					break;
				case 3:
					if (cGETimer == 0) {
						currentGlobalEffect = 0;
						floor.ignore_floor = false;
						adjustDifficulty ();
					}
					break;
				default:
					break;
				}
			}
			if (cL.lives == 0)
				Pause(1);
            if (egg_spawn_break <= 0) {
                int random_coop;
				int egg_type;
                random_coop = UnityEngine.Random.Range(0, 4);
                if (coopScripts[random_coop].signal_egg_spawn) {
                    GameObject new_egg = Instantiate(egg, transform.position, Quaternion.identity);
                    new_egg.transform.SetParent(gameScreen.transform, false);
                    new_egg.transform.localScale = new Vector3(screen_width_adjuster, screen_height_adjuster, 0);
                    new_egg.transform.localPosition = coops[random_coop].transform.localPosition;
					// TODO: Fix this!
					new_egg.transform.localPosition.Set(
						new_egg.transform.localPosition.x, 
						new_egg.transform.localPosition.y - 80,
						new_egg.transform.localPosition.z);
                    //new_egg.GetComponent<eggScript>().spawnPoint = random_coop;
					egg_type = spawnRandomEgg ();
					new_egg.GetComponent<eggScript> ().egg_type = egg_type;
					new_egg.GetComponent<eggScript> ().Start ();
                    new_egg.layer = 8;
					new_egg.GetComponent<eggScript> ().id = global_egg_ids;
					new_egg.GetComponent<Rigidbody2D> ().gravityScale = gravityScale;
					global_egg_ids++;
                    spawnedEggs.Add(new_egg);
					coopScripts[random_coop].signal_egg_spawn = false;
					coopScripts [random_coop].signalSound ();
					egg_spawn_break = current_spawn_rate;
                }
            } else
                egg_spawn_break--;
			coop_animation_trigger = UnityEngine.Random.Range (0, 180);
			if (coop_animation_trigger > 178) {
				coopScripts [UnityEngine.Random.Range (0, 4)].signalAnimation ();
			}
        }
    }

    public void Pause (int reason) {   // 0 for menu, 1 for game over.
        is_paused = true;
		menu.enabled = true;
		Time.timeScale = 0f;
        switch (reason) {
		case 0: // Menu.
			new_game = false;
			ad_button.interactable = false;
			gameScreen.GetComponent<CanvasGroup> ().interactable = false;
			gSwitchT.text = "Game Paused.";
			gScoreT.text = "Score " + hS.score;
            break;
		case 1: // Game Over!
			gameSwitchButton.changeToNewGame (true);
            Leaderboards.HighScore.SubmitScore(hS.score);
            if (hS.score > high_score) {
				highScoreT.text = ("Old High Score " + high_score);
				gScoreT.text = "New High Score! " + hS.score;
				high_score = hS.score;
				PlayerPrefs.SetInt ("High Score", hS.score);
				PlayerPrefs.Save ();
				hS.playHighScore ();
			} else {
				gScoreT.text = "Score " + hS.score;
				gameSwitchPanel.GetComponent<AudioSource> ().Play ();
			}
			if (adEnabled) {
				ad_button.interactable = true;
				adEnabled = false;
			} else {
				ad_button.interactable = false;
			}
			new_game = true;
			gameScreen.GetComponent<CanvasGroup> ().interactable = false;
			gSwitchT.text = "Game Over!";
            break;
        default:
            break;
        }
    }

    public void Continue () {
		Time.timeScale = 1f;
		is_paused = false;
		menu.enabled = false;
		gameScreen.GetComponent<CanvasGroup>().interactable = true;

		if (new_game) {
			foreach (GameObject egg in spawnedEggs)
				if (egg)
					Destroy (egg);
			cL.initializeLives (day_streak_lives);
			Start ();
		}
    }

	public void eggCaught(int ramp_nr, int egg_type, int egg_id) {
		int points;
		total_eggs_caught++;
		points = 1 * cM.multiplier;
		hS.score += points;
		hS.updateScore ();
		handleMultiplierChange ();
		resetEggCount (egg_id);
		switch(egg_type) {
		// Normal egg caught.
		case 0:
			break;
		// Life regeneration egg.
		case 1:
			if (cL.lives < cL.maxLives) {
				cL.addLife ();
				newFloatingText (2);
			}
			break;
		// Increase maximum lives by 1.
		case 2:
			if (cL.maxLives < 7) {
				cL.addLifeMax ();
				newFloatingText (3);
			}
			if (cL.lives < cL.maxLives) {
				cL.addLife ();
			}
			break;
		// This egg takes one life away.
		case 3:
			cL.removeLife ();
			newFloatingText (7);
			break;
		// This egg takes one MAX life away.
		case 4:
			cL.removeLifeMax ();
			newFloatingText (8);
			break;
		// Big egg, worth 5 points.
		case 5:
			points = 4 * cM.multiplier;
			hS.score += points;
			hS.updateScore ();
			newFloatingText (4);
			break;
		// Super egg, worth 20 points.
		case 6:
			points = 19 * cM.multiplier;
			hS.score += points;
			hS.updateScore ();
			newFloatingText (40);
			break;
		// Multiplier egg. Increases multiplier by 1.
		case 7:
			eggs_needed = cM.increaseMultiplier (1);
			cM.updateMultiplier ();
			adjustDifficulty ();
			newFloatingText (5);
			break;
		// Multiplier egg. Increases multiplier by 3.
		case 8:
			eggs_needed = cM.increaseMultiplier (3);
			cM.updateMultiplier ();
			adjustDifficulty ();
			newFloatingText (50);
			break;
		// Multiplier egg. Increases multiplier by 10.
		case 9:
			eggs_needed = cM.increaseMultiplier (10);
			cM.updateMultiplier ();
			adjustDifficulty ();
			newFloatingText (51);
			break;
		// Decreases multiplier by 1.
		case 10:
			eggs_needed = cM.decreaseMultiplier (1);
			cM.updateMultiplier ();
			adjustDifficulty ();
			newFloatingText (6);
			break;
		// Decreases multiplier by 3.
		case 11:
			eggs_needed = cM.decreaseMultiplier (3);
			cM.updateMultiplier ();
			adjustDifficulty ();
			newFloatingText (61);
			break;
		// Decreases multiplier to 1.
		case 12:
			eggs_needed = cM.decreaseMultiplier (19);
			cM.updateMultiplier ();
			adjustDifficulty ();
			newFloatingText (62);
			break;
		// Low Gravity Egg. Makes eggs fall at half speed for 5 seconds.
		case 13:
			resetEggGravityScale (15);
			current_spawn_rate = 120;
			currentGlobalEffect = 1;
			cGETimer = 600;
			newFloatingText (0);
			break;
		// Super mode egg. Grants double gravity and spawn rate. No drop penalty for 5 seconds.
		case 14:
			resetEggGravityScale (100);
			currentGlobalEffect = 2;
			floor.ignore_floor = true;
			cGETimer = 1200;
			newFloatingText (1);
			break;
		}
    }

	void adjustDifficulty() {
		if (currentGlobalEffect == 1) {
			current_spawn_rate = 120;
			return;
		} else if (currentGlobalEffect == 2)
			return;
		int game_minutes_total = updates_since_start / 3600;
		int gmt_spawn_rate = game_minutes_total * 2;
		int gmt_egg_gravity_scale = game_minutes_total * 10;

		if (cM.multiplier < 5) {
			current_spawn_rate = 100;
			resetEggGravityScale (30 + gmt_egg_gravity_scale);
		} else if (cM.multiplier < 9) {
			current_spawn_rate = 80;
			resetEggGravityScale (40 + gmt_egg_gravity_scale);
		} else if (cM.multiplier < 13) {
			current_spawn_rate = 60;
			resetEggGravityScale (50 + gmt_egg_gravity_scale);
		} else if (cM.multiplier < 17) {
			current_spawn_rate = 50;
			resetEggGravityScale (60 + gmt_egg_gravity_scale);
		} else {
			current_spawn_rate = 40;
			resetEggGravityScale (70 + gmt_egg_gravity_scale);
		}
		current_spawn_rate -= gmt_spawn_rate;
	}

	void handleMultiplierChange() {
		eggs_needed--;
		if(eggs_needed == 0) {
			eggs_needed = cM.increaseMultiplier (1);
			if (eggs_needed != 9999 && eggs_needed != 5) {
				cM.updateMultiplier ();
				adjustDifficulty ();
				newFloatingText (5);
			}
		}
	}

	void resetEggGravityScale(float new_value) {
		gravityScale = new_value;
		foreach (GameObject egg in spawnedEggs) {
			if(egg != null)
				egg.GetComponent<Rigidbody2D> ().gravityScale = new_value;
		}
	}

	public void resetEggCount(int egg_id) {
		foreach(GameObject egg in spawnedEggs) {
			if (egg != null && egg.GetComponent<eggScript> ().id == egg_id) {
				spawnedEggs.Remove (egg);
				break;
			}
		}
	}

	public void showAd() {
		if (Advertisement.IsReady("rewardedVideo")) {
			var options = new ShowOptions { resultCallback = afterAd };
			Advertisement.Show ("rewardedVideo", options);
		}
	}

	void afterAd(ShowResult result) {
		switch (result) {
		case ShowResult.Finished:
			adEnabled = false;
			new_game = false;
			ad_button.interactable = false;
			gSwitchT.text = "Game Paused.";
			gameSwitchButton.changeToNewGame (false);
			foreach (GameObject egg in spawnedEggs)
				if (egg)
					Destroy (egg);
			cL.afterAd();
			break;
		case ShowResult.Skipped:
			break;
		case ShowResult.Failed:
			break;
		}
	}

	int spawnRandomEgg() {
		int[] values = {
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			1,1,1, 
			2,2, 
			3,3,3, 
			4,4, 
			5,5,5,5,5,5,5,5,5,5,
			6,6, 
			7,7,7,
			8,8,
			9, 
			10,10,10,
			11,11,
			12,
			13,13,
			14};
		int selected = values[UnityEngine.Random.Range(0, values.Length)];
		if (currentGlobalEffect > 0 && (selected == 13 || selected == 14)) {
			selected = 0;
		}

		return selected;
	}

	void newFloatingText(int the_case) {
		GameObject new_floating_text = Instantiate(floating_text, transform.position, Quaternion.identity);
		if (the_case == 5 || the_case == 50 || the_case == 51 || the_case == 6 || the_case == 60 || the_case == 61 || the_case == 62) {
			new_floating_text.transform.SetParent (multiplierTextPlaceholder.transform, false);
		} else {
			new_floating_text.transform.SetParent (gameScreen.transform, false);
		}
		new_floating_text.transform.localScale = new Vector3(screen_width_adjuster, screen_height_adjuster, 0);
		new_floating_text.transform.GetComponent<floatingTextScript> ().text_case = the_case;
	}

	public void toggleTips(bool value) {
		tipsPanel.SetActive (value);
	}

	public void updateVolume() {
		AudioListener.volume = volume_slider.value;
        PlayerPrefs.SetFloat("volume", AudioListener.volume);
	}

	public void toggleDaily(bool value) {
		dailyRewardPanel.SetActive (value);
		Time.timeScale = 1f;
		menuButton.GetComponent<Button> ().interactable = true;
		cL.initializeLives (day_streak_lives);
	}

	void calculateDaysForDaily() {
		String last_date = PlayerPrefs.GetString ("Last Date Played");
		int day_streak = PlayerPrefs.GetInt ("Day Streak");
		double calculate_hours;
		if (last_date == "")
			last_date_played = today;
		else 
			last_date_played = DateTime.Parse(last_date);
		int days_delta = (today.Day - last_date_played.Day);
		int months_delta = (today.Month - last_date_played.Month);
		if (days_delta < 0) {
			switch (months_delta) {
			case 0:	// DAAAAAM, you played last year this month!!!
				break;
			case 1:	// Played last the month before.
				calculate_hours = (today - last_date_played).TotalHours;
				if (calculate_hours < 25) {
					days_delta = 0;
					day_streak++;
				} else
					day_streak = 0;
				break;
			case -11: // Played in december last.
				calculate_hours = (today - last_date_played).TotalHours;
				if (calculate_hours < 25) {
					days_delta = 0;
					day_streak++;
				} else
					day_streak = 0;
				break;
			default: // Played last year or more than 1 month ago. 
				break;
			}
		}

		switch(days_delta) {
		case 0:
			break;
		case 1:
			if(months_delta == 0)
				day_streak++;
			else
				day_streak = 0;
			break;
		default:
			day_streak = 0;
			break;
		}
		PlayerPrefs.SetString ("Last Date Played", today.ToString());
		PlayerPrefs.SetInt ("Last Day Played", today.Day);
		PlayerPrefs.SetInt ("Last Month Played", today.Month);
		PlayerPrefs.SetInt ("Day Streak", day_streak);
		PlayerPrefs.Save ();
		day_streak_lives = 3 + day_streak;
		if (day_streak_lives > 7)
			day_streak_lives = 7;
		dailyRewardPanel.GetComponent<dailyRewardScript> ().setDays (day_streak);
		if(day_streak > 4)
			dailyRewardPanel.GetComponent<dailyRewardScript> ().setLives (4);
		else
			dailyRewardPanel.GetComponent<dailyRewardScript> ().setLives (day_streak);
	}
}
