﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class floorScript : MonoBehaviour {

    public bool ignore_floor = false;
    public int egg_splatter = 0;    // 0 for no eggs, 1+ for multiple broken eggs.
    public bool decrease_cash = false;
	eggScript currentEgg;
	public gameScript game_script;
	AudioSource my_source;

	void Start() {
		my_source = gameObject.GetComponent<AudioSource>();
	}

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "egg") {
			my_source.Play ();
			col.gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Static;
			currentEgg = col.gameObject.GetComponent<eggScript> ();
			currentEgg.self_destruct = 30;
			col.gameObject.GetComponent<Image> ().sprite = currentEgg.splatterType;
			game_script.resetEggCount (currentEgg.id);
			if (currentEgg.egg_type == 3 || currentEgg.egg_type == 4 || currentEgg.egg_type == 10
				|| currentEgg.egg_type == 11 || currentEgg.egg_type == 12) {
				return;
			}
			if(!ignore_floor)
            	decrease_cash = true;
        }
    }
}
