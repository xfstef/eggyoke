﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameSwitchButtonScript : MonoBehaviour {

	public Sprite buton_continue;
	public Sprite buton_new_game;

	public void changeToNewGame(bool new_game) {
		if (new_game) {
			this.gameObject.GetComponent<Image>().sprite = buton_new_game;
		} else {
			this.gameObject.GetComponent<Image>().sprite = buton_continue;
		}
	}
}
