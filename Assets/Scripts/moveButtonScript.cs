﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class moveButtonScript : MonoBehaviour {

	public Sprite selected;
	public Sprite notSelected;
    public catcherScript theCatcher;
    public int myPosition;
    public Image myImage;
    public Color pushedColor;
	
	public void changeSelection (bool isSelected) {
		if (isSelected) {
            myImage.sprite = selected;
            myImage.color = pushedColor;

        } else {
            myImage.sprite = notSelected;
            myImage.color = Color.white;
		}
	}
}
