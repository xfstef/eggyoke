﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class highScoreScript : MonoBehaviour {

    public int score = 0;   // Current max score.
	AudioSource my_source;

	void Start() {
		my_source = gameObject.GetComponent<AudioSource>();
	}

	public void updateScore() {  // Returns true if game over!
		this.gameObject.GetComponent<Text>().text = score.ToString();
	}

	public void playHighScore() {
		my_source.Play ();
	}
}
