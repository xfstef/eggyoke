﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class catchHelpScript : MonoBehaviour {

    public int ramp_nr = 0;
    public gameScript game_script;
	AudioSource my_source;

	void Start() {
		my_source = gameObject.GetComponent<AudioSource>();
	}

	void OnTriggerEnter2D(Collider2D col) {
		eggScript the_egg = col.gameObject.GetComponent<eggScript> ();
		int the_egg_type = the_egg.egg_type;
		int egg_id = the_egg.id;
		int spawn_point = the_egg.spawnPoint;
		if (ramp_nr != spawn_point) {
			return;
		}
		my_source.Play ();
		game_script.eggCaught(ramp_nr, the_egg_type, egg_id);
		Destroy(col.gameObject);
	}
}
