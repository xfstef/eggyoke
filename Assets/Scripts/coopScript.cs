﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coopScript : MonoBehaviour {

    public int position = 0;    // 0 BL, 1 BR, 2 TL, 3 TR.
    public bool signal_egg_spawn = false;
    int ticks_last_egg = 1;
	AudioSource my_source;
	bool is_animating = false;
	int animation_wait = 35;
	int animation_type = 0;
	Animator my_animations;

	// Use this for initialization
	void Start () {
		my_source = gameObject.GetComponent<AudioSource>();
		my_animations = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		ticks_last_egg++;
		if (ticks_last_egg > 15 && !signal_egg_spawn) {
			signal_egg_spawn = true;
		}
		if (is_animating) {
			if (animation_wait > 0) {
				animation_wait--;
			} else {
				is_animating = false;
			}
		}
	}

	public void signalSound() {
		my_source.Play ();
	}

	public void signalAnimation() {
		if (!is_animating) {
			is_animating = true;
			animation_type = Random.Range (1, 5);
			switch (animation_type) {
			case 1:
				my_animations.Play ("coop1");
				animation_wait = 50;
				break;
			case 2:
				my_animations.Play ("coop2");
				animation_wait = 50;
				break;
			case 3:
				my_animations.Play ("coop3");
				animation_wait = 50;
				break;
			case 4:
				my_animations.Play ("coop4");
				animation_wait = 50;
				break;
			default:
				break;
			}
		}
	}
}
