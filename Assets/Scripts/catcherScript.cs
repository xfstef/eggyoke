﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class catcherScript : MonoBehaviour {

    public int catcher_pos = 0; // 0 Bottom left, 1 Bottom right, 2 for Top left, 3 for Top right.
    GameObject[] catcher_zones;
	AudioSource my_source;
	public GameObject bTL;
	public GameObject bTR;
	public GameObject bBL;
	public GameObject bBR;
	Animator move_animations;

    public void moveTo(int position) {
		my_source.Play ();
        switch (position) {
		case 1:
			catcher_zones [0].SetActive (true);
			catcher_zones [1].SetActive (false);
			catcher_zones [2].SetActive (false);
			catcher_zones [3].SetActive (false);
			move_animations.Play ("mKLD");
			bTL.GetComponent<moveButtonScript> ().changeSelection (false);
			bTR.GetComponent<moveButtonScript> ().changeSelection (false);
			bBL.GetComponent<moveButtonScript> ().changeSelection (true);
			bBR.GetComponent<moveButtonScript> ().changeSelection (false);
            break;
		case 2:
			catcher_zones [0].SetActive (false);
			catcher_zones [1].SetActive (true);
			catcher_zones [2].SetActive (false);
			catcher_zones [3].SetActive (false);
			move_animations.Play("mKRD");
			bTL.GetComponent<moveButtonScript> ().changeSelection (false);
			bTR.GetComponent<moveButtonScript> ().changeSelection (false);
			bBL.GetComponent<moveButtonScript> ().changeSelection (false);
			bBR.GetComponent<moveButtonScript> ().changeSelection (true);
            break;
		case 3:
			catcher_zones [0].SetActive (false);
			catcher_zones [1].SetActive (false);
			catcher_zones [2].SetActive (true);
			catcher_zones [3].SetActive (false);
			move_animations.Play("mKLU");
			bTL.GetComponent<moveButtonScript> ().changeSelection (true);
			bTR.GetComponent<moveButtonScript> ().changeSelection (false);
			bBL.GetComponent<moveButtonScript> ().changeSelection (false);
			bBR.GetComponent<moveButtonScript> ().changeSelection (false);
            break;
		case 4:
			catcher_zones [0].SetActive (false);
			catcher_zones [1].SetActive (false);
			catcher_zones [2].SetActive (false);
			catcher_zones [3].SetActive (true);
			move_animations.Play("mKRU");
			bTL.GetComponent<moveButtonScript> ().changeSelection (false);
			bTR.GetComponent<moveButtonScript> ().changeSelection (true);
			bBL.GetComponent<moveButtonScript> ().changeSelection (false);
			bBR.GetComponent<moveButtonScript> ().changeSelection (false);
            break;
        default:
            catcher_zones[0].SetActive(false);
            catcher_zones[1].SetActive(false);
            catcher_zones[2].SetActive(false);
            catcher_zones[3].SetActive(false);
			bTL.GetComponent<moveButtonScript> ().changeSelection (false);
			bTR.GetComponent<moveButtonScript> ().changeSelection (false);
			bBL.GetComponent<moveButtonScript> ().changeSelection (false);
			bBR.GetComponent<moveButtonScript> ().changeSelection (false);
            break;
        }
    }

	// Use this for initialization
	void Start () {
        catcher_zones = GameObject.FindGameObjectsWithTag("catcherZone");
        Array.Sort(catcher_zones, delegate (GameObject cz1, GameObject cz2) { return cz1.name.CompareTo(cz2.name); });
        catcher_zones[0].SetActive(false);
        catcher_zones[1].SetActive(false);
        catcher_zones[2].SetActive(false);
        catcher_zones[3].SetActive(false);
		my_source = gameObject.GetComponent<AudioSource>();
		move_animations = gameObject.GetComponent<Animator> ();
    }

	void FixedUpdate() {

	}
}
